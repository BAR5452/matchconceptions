<!DOCTYPE HTML>
<?php $backface="<img class='back-face' src='data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiPgogIDxkZWZzPgogICAgPHBhdHRlcm4gaWQ9ImRpYWdvbmFsIiBwYXR0ZXJuVW5pdHM9InVzZXJTcGFjZU9uVXNlIiB3aWR0aD0iMzAlIiBoZWlnaHQ9IjUlIiBwYXR0ZXJuVHJhbnNmb3JtPSJyb3RhdGUoMzA1KSI+CiAgICAgIDx0ZXh0IHg9IjAiIHk9IjMwIiBmb250LXNpemU9IjMwIiBmb250LWZhbWlseT0ic2Fucy1zZXJpZiIgZmlsbD0iIzAwMDAwMCI+TWF0Y2hjb25jZXB0aW9uczwvdGV4dD4KICAgIDwvcGF0dGVybj4KICA8L2RlZnM+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0idXJsKCNkaWFnb25hbCkiIC8+Cjwvc3ZnPg==' alt='' />"; ?>
<html lang="de">
<head>
  <meta charset="UTF-8">

  <title>Matchconceptions Memory Game</title>

  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <section class="memory-game">
    <div class="memory-card" data-framework="A">
      <?php 
        $text = 'A';
        $width = 640 * 0.25 - 10;
        $height = 640 * 0.33333 - 10;
        $textSize = min($width, $height) * 0.1;
        $style = 'background-color: #FFCCCB; font-size: ' . $textSize . 'px; font-family: sans-serif; fill: #000000; text-anchor: middle; dominant-baseline: middle; word-wrap: break-word; text-align: center;';

        $words = explode(' ', $text);
        $lines = [];
        $currentLine = '';

        foreach ($words as $word) {
          $testLine = $currentLine . ' ' . $word;
          $testWidth = $textSize * strlen($testLine) / 2;
          if ($testWidth > $width) {
            $lines[] = $currentLine;
            $currentLine = $word;
          } else {
            $currentLine = $testLine;
          }
        }

        $lines[] = $currentLine;

        $textElements = '';
        $numLines = count($lines);
        $lineHeight = $height / $numLines;
        $yPos = $lineHeight / 2;

        foreach ($lines as $line) {
          $textElements .= "<text x='50%' y='$yPos' style='$style'>$line</text>";
          $yPos += $lineHeight;
        }

        $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'>$textElements</svg>";
        $svgData = base64_encode($svg);
        $src = 'data:image/svg+xml;base64,' . $svgData;
        $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>";

        echo $imgTag;
      ?>


      <?php $text = 'A'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/aurelia.svg" alt="Aurelia" style="background-color: #FFCCCB"/> -->
      <!-- <img class="back-face" src="img/js-badge.svg" alt="" /> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="A">
    <?php $text = 'A'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/aurelia.svg" alt="Aurelia" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>

    <div class="memory-card" data-framework="B">
      <?php $text = 'B'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/vue.svg" alt="Vue" style="background-color: #FFCCCB"/> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="B">
      <?php $text = 'B'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/vue.svg" alt="Vue" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>

    <div class="memory-card" data-framework="C">
      <?php $text = 'C'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/angular.svg" alt="Angular" style="background-color: #FFCCCB"/> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="C">
      <?php $text = 'C'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/angular.svg" alt="Angular" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>

    <div class="memory-card" data-framework="D">
      <?php $text = 'D'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/ember.svg" alt="Ember" style="background-color: #FFCCCB"/> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="D">
      <?php $text = 'D'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/ember.svg" alt="Ember" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>

    <div class="memory-card" data-framework="E">
      <?php $text = 'E'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/backbone.svg" alt="Backbone" style="background-color: #FFCCCB"/> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="E">
      <?php $text = 'E'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/backbone.svg" alt="Backbone" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>

    <div class="memory-card" data-framework="F">
      <?php $text = 'F'; $style = 'background-color: #FFCCCB'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/react.svg" alt="React" style="background-color: #FFCCCB"/> -->
      <?php echo $backface; ?>
    </div>
    <div class="memory-card" data-framework="F">
      <?php $text = 'F'; $style = 'background-color: #90EE90'; $width = 640 * 0.25 - 10; $height = 640 * 0.33333 - 10; $textSize = min($width, $height) * 0.4; $svg = "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'><text x='50%' y='50%' text-anchor='middle' alignment-baseline='middle' font-size='$textSize'>$text</text></svg>";  $svgData = base64_encode($svg); $src = 'data:image/svg+xml;base64,'.$svgData; $imgTag = "<img class='front-face' src='$src' alt='$text' style='$style'/>"; echo $imgTag; ?>
      <!-- <img class="front-face" src="img/react.svg" alt="React" style="background-color: #90EE90"/> -->
      <?php echo $backface; ?>
    </div>
  </section>

  <script src="scripts.js"></script>

  <!--- From https://marina-ferreira.github.io/tutorials/js/memory-game/ --->
</body>
</html>
